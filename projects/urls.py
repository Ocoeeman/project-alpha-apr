from django.urls import path
from projects.views import get_all_from_project, show_project, create_project


urlpatterns = [
    path("", get_all_from_project, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
